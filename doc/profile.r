M<-100             # Как у автора
ps<- 1/(M:2)^2    # вектор вероятностей появления отдельных значений; пока в каких-то относительных единицах
ps<-ps/sum(ps)    # Нормируем сумму вероятностей на единицу
ss<-c(0,cumsum(ps))    # Строим вектор границ инервалов


L<-length(ps)+1
rs<-c(1,sapply(1:(L-1), function(k,ss,L){min(which(ss>k/L))-1}, ss=ss,L=L))

get_shift<-function(alpha,val,lbs,ss) {
  return (as.numeric(cut(alpha,ss[max(1,val-1):length(ss)],
                              lbs[max(1,val-1):length(lbs)])))
}

vshift<-Vectorize(get_shift,c("alpha","val"))

gen_chen <- function(n,ss,rs,lbs) {
  alpha<-runif(n)
  val<-rs[floor(alpha*length(rs))+1]
  val = vshift(alpha,val,ss=ss,lbs=lbs)+val-1
  return (val)
}

lbs=1:(M-1)  # Это "метки на кубике" (вектор) -- те самые дискретные значения, которые пойдут на выход 

get_vladob <- function(n, brks=ss, labels=lbs) {
  val<-as.numeric(cut(runif(n), breaks=brks, labels))
  return(val)
}

st<-system.time(val<-get_vladob(100000, ss, lbs))
hist(val)
print(st)
st<-system.time(val<-gen_chen(100000,ss,rs,lbs))
hist(val)
print(st)
