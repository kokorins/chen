#include <chen/chen.h>

void generate_million()
{
  using namespace chen;
  std::vector<double> p(3, 1./3);
  Chen ch(p);
  size_t len = 100000;
  std::vector<double> res;
  std::generate_n(std::back_inserter(res), len, ch);
  double mean = std::accumulate(res.begin(), res.end(), 0.0)/res.size();
  double sd = std::inner_product(res.begin(), res.end(), res.begin(), 0.0)/res.size();
  std::cout<<"mean: "<<mean<<" var: "<<sd-mean*mean<<std::endl;
}

int main()
{
  generate_million();
  return 0;
}
