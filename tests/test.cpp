#define BOOST_TEST_MODULE ChenTests
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <chen/chen.h>
#include <boost/test/unit_test.hpp>
#include <iostream>

struct iota {
  int idx_;
public:
  iota(int idx):idx_(idx){}
  iota():idx_(0){}
  int operator()() {
    return idx_++;
  }
};

BOOST_AUTO_TEST_CASE(ChenUniform)
{
  using namespace chen;
  std::vector<double> p(3, 1./3);
  Chen ch(p);
  size_t len = 100000;
  std::vector<double> res(p.size());
  for(size_t i=0; i<len; ++i) {
    ++res[ch.next()];
  }
  for(size_t i=0; i<p.size(); ++i)
    BOOST_CHECK_CLOSE(res[i], len*p[i], 1);
}

BOOST_AUTO_TEST_CASE(ChenGen)
{
  using namespace chen;
  std::vector<double> p(10);
  p[0]=0.5;
  p[1]=0.2;
  p[2]=0.05;
  p[3]=0.05;
  p[4]=0.1;
  p[5]=0.03;
  p[6]=0.02;
  p[7]=0.04;
  p[8]=0.005;
  p[9]=0.005;
  Chen ch(p);
  size_t len = 100000;
  std::vector<double> res(p.size());
  for(size_t i=0; i<len; ++i)
    ++res[ch.next()];
  for(size_t i=0; i<p.size(); ++i)
    BOOST_CHECK_CLOSE(res[i], len*p[i], 5);
}

BOOST_AUTO_TEST_CASE(ChenLookup)
{
  using namespace chen;
  std::vector<double> p(3, 1./3);
  Chen ch(p);
  double val = ch.next();
  BOOST_CHECK_CLOSE(ch.lookup(), val, 1.0);
}
