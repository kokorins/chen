#ifndef CHEN_H_
#define CHEN_H_
#include <vector>
#include <boost/random.hpp>
/// \file chen.h
/// \mainpage Chen discrete finite distribution generator algorithm
namespace chen
{
/**
 * Method for fast generation of discrete finite distributions
 * set as a vector of probabilities \f$ p_1, ..., p_n, \sum p_i = 1, 0 \leq p_i\leq 1, \forall i \f$
 * The algorithm works in O(1) in average and needs 1 alpha with same p_i,
 * The preparation (set p_i) is O(n), where n is number of probabilities.
 */
class Chen {
public:
  explicit Chen(const std::vector<double>& probs);
  size_t operator()();
  /**
   * Generates new pseudo-random value
   * \return is 0..n-1
   */
  size_t next();
  /**
   * Return the already created value
   * \sa next()
   */
  size_t lookup()const;
  void setProbs(const std::vector<double>& p);
  void setSeed(int seed) const;
private:
  size_t _cur_value;
  std::vector<double> _p;
  std::vector<double> _s;
  size_t _param;
  std::vector<size_t> _r;
  boost::uniform_01<> _alpha;
  mutable boost::minstd_rand _gen;
};
}//namespace chen
#endif
