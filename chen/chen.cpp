#include <cmath>
#include "chen.h"
#include <numeric>

namespace chen {

Chen::Chen(const std::vector<double>&probs): _param(probs.size()+1)
{
  setProbs(probs);
  next();
}

size_t Chen::operator ()()
{
  return next();
}

size_t Chen::next()
{
  double alpha = _alpha(_gen);
  size_t j=(size_t)std::floor(_param*alpha);
  size_t i=_r[j];
  while(alpha>=_s[i])
    ++i;
  _cur_value=i;
  return _cur_value;
}

void Chen::setProbs(const std::vector<double>&probs)
{
  _p = probs;
  _s = _p;
  std::partial_sum(_s.begin(),_s.end(),_s.begin());//s_i=sum(p_1+...p_i)
  int i = 0;
  double t = 0;
  _param = probs.size()+1;
  _r.resize(_param);
  double revparam = 1./_param;
  for(size_t j=0; j<_param; ++j) {
    while(_s[i]<=t)
      ++i;
    _r[j] = i;
    t += revparam;
  }
}

void Chen::setSeed(int seed)const
{
  _gen.seed(seed);
}

size_t Chen::lookup() const
{
  return _cur_value;
}
} //namespace chen
